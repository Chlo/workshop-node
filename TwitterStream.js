const Twitter = require('Twitter');
const { Readable } = require('stream');

class TwitterStream extends Readable {

	constructor(cfg, keyword){
		super({ objectMode : true});
		this.client = new Twitter(cfg);
		this.connect(keyword)
	}

	_read() {}

	destroy(){
		this.stream.destroy()
	}

	connect(query){

		this.stream = this.client.stream('statuses/filter', {track: query});
		this.stream.on("data", tweet => this.push(tweet));
        this.stream.on('error', error => this.push(error));
	}
}

module.exports = TwitterStream;